﻿using System;
using System.Collections.Generic;
namespace Coding_examples
{
    public abstract class TaxCalculator
    {
        public abstract double TaxPercentage { get;  }
        public abstract List<string> ExcemptProductCategories { get; }
        public abstract double calculate_tax(double price, string category);


    }

    public class BritishColumbiaTaxCalculator : TaxCalculator
    {
        override public double TaxPercentage { get; } = 12;

        override public List<string> ExcemptProductCategories { get; } = new List<string>(new string[]
                    {
                        "groceries", "transport", "health", "lodging", "books", "magazines"
                    });

        override public double calculate_tax(double price, string category)
        {
            double result = 0;
            if (!ExcemptProductCategories.Contains(category))
            {
                result = Math.Round(price * TaxPercentage / 100, 2);
            }

            return result;
        }

    }

    public class QuebecCalculator : TaxCalculator
    {
        override public double TaxPercentage { get; } = 14.975;
        override public List<string> ExcemptProductCategories { get; } = new List<string>(new string[]
                    {
                        "groceries", "transport", "health", "lodging", "books", "magazines"
                    });

        override public double calculate_tax(double price, string category)
        {
            double result = 0;
            if (!ExcemptProductCategories.Contains(category))
            {
                result = Math.Round(price * TaxPercentage / 100, 2);
            }

            return result;
        }

    }
}
