﻿using System;
using System.Collections.Generic;
namespace Coding_examples
{
    class TaxCalculator
    {
        private string Province { get; set; }
        private double TaxPercentage { get; set; }
        private List<string> ExcemptProductCategories { get; set; }
        public TaxCalculator(string province)
        {
            Province = province;

            switch(province)
            {
                case "Quebec":
                    Province = "Quebec";
                    TaxPercentage = 14.975;
                    ExcemptProductCategories = new List<string> (new string[]
                    {
                        "groceries", "transport", "health", "lodging"
                    });
                    break;
                default:
                    Province = "BritishColumbia";
                    TaxPercentage = 12;
                    ExcemptProductCategories = new List<string>(new string[]
                    {
                        "groceries", "transport", "health", "lodging", "books", "magazines"
                    });
                    break;
            }
            
        }
        public double calculate_tax(double price, string category)
        {
            double result = 0;
            if (!ExcemptProductCategories.Contains(category))
            {
                result = Math.Round(price * TaxPercentage / 100, 2);
            } 

            return result;
        }

    }
}
